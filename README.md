# Flights and COVID-19 - BDT project - group20

A big data system of flight search, which considers Covid-19 data to predict the future number of flights.

This is the project of the Group 20 - Francesca Andolfatto, Raffaele Marchesi - for the Big Data Technologies course of the University of Trento.


### Architecture

The project is designed to run entirely on the Datatabricks web-based platform. No local installation is required to run it. We have created a modular big data pipeline through IPython-style notebooks, which takes full advantage of Databricks' integrated technologies like Spark, Delta Lake and the automated claster management, to easily handle large amounts of data.


### Structure

The project is made up of two components: the ETL and the analysis

 - `etl`: contains the notebooks necessary to extract data from our sources, transform it by aggregating and cleaning it to obtain the desired schema and load it to Delta Lake. In this phase the notebooks are designed as a pipeline to be run without interactions; they do not contain markdown or visualizations, to optimize the efficiency of Spark's distributed work. But it is still possible to uncomment some sections of code to follow the transformation of the data through tables and schemas, reducing performance.
    - `run_notebooks_etl`: this notebook does not work on the community edition of Databricks. It automates the etl process, running its notebooks  in the correct sequence. If you don't have a premium version of Databricks you need to perform its function manually, running the following notebooks one at a time.
    - `extract_flights`: downloads the flights data from the [OpenSky Network](https://opensky-network.org/)
    - `merge_flights`: merge the flights data, that are relised monthly
    - `extract_airports`: downloads the airports dataset and the countries dataset from [OpenFlights](https://openflights.org/)
    - `extract_covid`: downloads the world wide covid distribution the [EU OpenData Portal](https://data.europa.eu/euodp/en/home)
    - `data_preparation`: merge the data together and transform it with the correct schema for the analysis, finally it loads the results in the Delta Lake 
 - `analysis`: contains the notebooks for the analysis process
    - `flight_covid_forecasting`: performs a time series analysis on the number of weekly flights of each country and uses the covid data as regressors to predict future trends. Forecasting is done with [Prophet](https://facebook.github.io/prophet/), using MLflow to manage and track models.
    - `dashboard`: finally, this last notebook can be run to create an interactive dashboard showing some visualization examples to present the data we have collected and the forecast results.


### Setup

To run this project just import the [BD-project-group20.dbc](../master/BD-project-group20.dbc) file on Databricks, it will load the project folder on your platform. Then run the notebooks, as presented above.





